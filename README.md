# Cluster backup script

This script can be used to backup all kubernetes resources in specified namespace or archive directories. It is using `dialog` to create its TUI. 

## Prerequisites

You need to install dialog and kubectl. You also have to configure the last one.

## Usage

Script has terminal user interface so you can easily navigate it using tab, arrows, space and enter. At this moment it doesn't support any flags. 

```sh
chmod +x backup.bash
./backup.bash
```
