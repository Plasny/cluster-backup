#!/bin/bash

### --- VARIABLES --------------------------------------------------------------
btitle="Cluster backup"
res_list="list.txt"
res_dir_name="resources"
data_dir_name="data"

### --- FUNCTION ---------------------------------------------------------------
# DialogGen is dialog wrapping function
function DialogGen {
	case $1 in
		dir_select) 
			dialog  --stdout --clear \
				--backtitle "$btitle" \
				--title "$2" \
				--dselect "$3" 40 60 ;;
		msgbox)
			dialog  --stdout --clear \
				--backtitle "$btitle" \
				--title "$2" \
				--msgbox "$3" 0 0 ;;
		input)
			dialog  --stdout --clear \
				--backtitle "$btitle" \
				--title "$2" \
				--inputbox "$3" 20 60 "$4" ;;
		info)
			dialog  --stdout \
				--backtitle "$btitle" \
				--title "$2" \
				--infobox "$3" 20 60 ;;
		command)
			dialog  --stdout \
				--backtitle "$btitle" \
				--title "$2" \
				--prgbox "$3" "$4" 30 60 ;;
		menu)
			dialog  --stdout \
				--backtitle "$btitle" \
				--title "$2" \
				--menu "$3" 0 0 0 $4 $5 $6 $7 $8 $9 ;;
	esac
}

# ------------------------------------------------------------------------------

# kubectlgetall looks for all resources of all types in given namespace
function KubectlGetAll {
	for i in $( kubectl api-resources --namespaced -o name | grep -v "events" | sort ); do
		tmp=$( kubectl get --ignore-not-found -n ${1} -o name ${i} 2>/dev/null | sort )
		if [ ! -z "$tmp" ]; then
			echo -e "\nResource:" $i
			for each in "${tmp[@]}"; do
				echo "$each"
			done
		fi
	done
}

# kubectlgetmore gets all resources metadata from the list and saves it
function KubectlGetMore {
	for i in $(cat $res_list); do
		case $i in
			"") ;;
			Resource:) cd $r_dir/${1} ;;
			*) 
				if [[ $i == */* ]]; then
					tmp=$( echo $i | awk 'BEGIN { FS="/" } { print $2 }' )
					kubectl get -n ${1} -o yaml $i > $tmp.yaml
				else

					mkdir -p $i
					cd $i
				fi;;
		esac
	done
}

function KubernetesResources {
	namespace=$( DialogGen input "Choose namespace to backup" \
		"Enter namespace name:" "selfhosted")
	[ $? = 1 ] && return # if cancel button is pressed return to main menu

	mkdir $r_dir/$namespace
	cd $r_dir/$namespace
	
	DialogGen info "Backing up kubernetes resources" \
		"Creating list of resources... This will take a moment"
	KubectlGetAll $namespace > $res_list
	
	DialogGen info "Backing up kubernetes resources" \
		"Saving resources metadata... This will take a moment"
	KubectlGetMore $namespace
}

# ------------------------------------------------------------------------------

function ArchiveDir {
	a_dir=$( DialogGen dir_select "Choose directory to archive" "/" )
	[ $? = 1 ] && return

	[[ $a_dir == */ ]] && a_dir=$( echo $a_dir | rev | cut -c2- | rev ) # remove trailing /
	
	a_name=$( echo $a_dir | awk 'BEGIN { FS="/" } { print $(NF) }' )
	
	compression=$( DialogGen menu "Compression menu" "Choose compression type" \
		0 "none" \
		1 "gzip" \
		2 "bzip2" )
	[ $? = 1 ] && return

	case $compression in
		0) time tar cvf $d_dir/$a_name.tar $a_dir ;;
		1) time tar cvfz $d_dir/$a_name.tar.gz $a_dir ;;
		2) time tar cvfj $d_dir/$a_name.tar.bz2 $a_dir ;;
	esac

	echo -e "\nArchive size:"
	ls -lh $d_dir/$a_name.* | awk '{ print $5 }'
	echo -e "\nPress enter to continue."
	read

}

### --- MAIN -------------------------------------------------------------------

# initial setup
wd=$( DialogGen dir_select "Choose directory to which archive will be saved" "/")
b_dir="$wd/cluster_backup"
r_dir="$b_dir/$res_dir_name"
d_dir="$b_dir/$data_dir_name"
mkdir -p $b_dir/{$res_dir_name,$data_dir_name}
	
while true; do
	menu=$( DialogGen menu "main menu" "Backup:" \
		1 "kubernetes-resources" \
		2 "specific-directory" )

	[ $? = 1 ] && break # if cancel button is pressed exit program

	case $menu in
		1) KubernetesResources;;
		2) ArchiveDir;;
	esac
done
